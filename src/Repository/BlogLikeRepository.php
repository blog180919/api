<?php

namespace App\Repository;

use App\Entity\Blog;
use App\Entity\BlogLike;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<BlogLike>
 *
 * @method BlogLike|null find($id, $lockMode = null, $lockVersion = null)
 * @method BlogLike|null findOneBy(array $criteria, array $orderBy = null)
 * @method BlogLike[]    findAll()
 * @method BlogLike[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BlogLikeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BlogLike::class);
    }

    public function save(BlogLike $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(BlogLike $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

//    /**
//     * @return BlogLike[] Returns an array of BlogLike objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('b')
//            ->andWhere('b.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('b.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?BlogLike
//    {
//        return $this->createQueryBuilder('b')
//            ->andWhere('b.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }

    public function findBlogLikesByBlog(Blog $blog, User $user): ?BlogLike
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.blog = :blog')
            ->andWhere('b.createdBy = :user')
            ->setParameter('blog', $blog)
            ->setParameter('user', $user)
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult()
            ;
    }
}
