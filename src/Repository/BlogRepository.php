<?php

namespace App\Repository;

use App\Entity\Blog;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Blog>
 *
 * @method Blog|null find($id, $lockMode = null, $lockVersion = null)
 * @method Blog|null findOneBy(array $criteria, array $orderBy = null)
 * @method Blog[]    findAll()
 * @method Blog[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BlogRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Blog::class);
    }

    public function save(Blog $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Blog $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

//    /**
//     * @return Blog[] Returns an array of Blog objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('b')
//            ->andWhere('b.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('b.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }


    public function findFilteredBlogs(string $authorName, int $categoryId): QueryBuilder
    {
        $queryBuilder = $this->createQueryBuilder('b2');
        $basisQueryBuilder = $this->createQueryBuilder('b')
            ->leftJoin('b.category', 'c')
            ->leftJoin('b.createdBy', 'u')
            ->leftJoin('u.person', 'up')
            ->andWhere('b.deletedAt IS NULL');

        if ($categoryId > 0) {
            $basisQueryBuilder->andWhere('c.id = :categoryId');
            $queryBuilder->setParameter('categoryId', $categoryId);
        }

        if ($authorName !== '') {
            $basisQueryBuilder->andWhere('up.firstName like :authorName');
            $queryBuilder->setParameter('authorName', '%' . $authorName . '%');
        }

        return $queryBuilder
            ->andWhere($queryBuilder->expr()->in('b2.id', $basisQueryBuilder->getDQL()));
    }

    public function blogsPagination(string $authorName, int $page, int $itemsPerPage, int $categoryId): array
    {
        return $this->findFilteredBlogs($authorName, $categoryId)
            ->select('b2')
            ->orderBy('b2.id', 'DESC')
            ->setFirstResult($itemsPerPage * ($page - 1))
            ->setMaxResults($itemsPerPage)
            ->getQuery()
            ->getResult();
    }

    public function getBlogsCount(): ?int
    {
        return $this->createQueryBuilder('b')
            ->select('COUNT(b.id)')
            ->getQuery()
            ->getSingleScalarResult()
            ;
    }

//    public function findOneBySomeField($value): ?Blog
//    {
//        return $this->createQueryBuilder('b')
//            ->andWhere('b.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }

    public function getLikesCount(int $id): ?int
    {
        return $this->createQueryBuilder('b')
            ->select('COUNT(bl.id)')
            ->leftJoin('b.blogLikes', 'bl')
            ->andWhere('b.id = :id')
            ->andWhere('bl.deletedAt IS NULL')
            ->setParameter('id', $id)
            ->getQuery()
            ->getSingleScalarResult()
        ;
    }

    public function getCommentsCount(int $id): ?int
    {
        return $this->createQueryBuilder('b')
            ->select('COUNT(bc.id)')
            ->leftJoin('b.comments', 'bc')
            ->andWhere('b.id = :id')
            ->andWhere('bc.deletedAt IS NULL')
            ->setParameter('id', $id)
            ->getQuery()
            ->getSingleScalarResult()
        ;
    }


    /**
     * @return Blog[] Returns an array of Post objects
     */
    public function findByUsers(array $usersIds): array
    {
        return $this->createQueryBuilder('b')
            ->leftJoin('b.createdBy', 'u')
            ->andWhere('u.id in (:ids)')
            ->andWhere('b.deletedAt IS NULL')
            ->setParameter('ids', $usersIds)
            ->orderBy('b.id', 'ASC')
            ->getQuery()
            ->getResult()
            ;
    }

    /**
     * @return Blog[] Returns an array of Blog objects
     */
    public function findBlogsByTrending(): array
    {
        return $this->createQueryBuilder('b')
            ->leftJoin('b.blogLikes', 'bl')
            ->andWhere('b.deletedAt IS NULL')
            ->groupBy('b.id')
            ->orderBy('COUNT(bl)', 'DESC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @return Blog[] Returns an array of Blog objects
     */
    public function findBlogsByCommentsCount(): array
    {
        return $this->createQueryBuilder('b')
            ->leftJoin('b.comments', 'bc')
            ->andWhere('b.deletedAt IS NULL')
            ->groupBy('b.id')
            ->orderBy('COUNT(bc)', 'DESC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
}
