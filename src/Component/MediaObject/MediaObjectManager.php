<?php

declare(strict_types=1);

namespace App\Component\MediaObject;

use App\Component\Core\AbstractManager;
use App\Entity\MediaObject;

/**
 * Class MediaObjectManager
 *
 * @method save(MediaObject $mediaObject, bool $needToFlush = false): void
 * @package App\Component\MediaObject
 */
class MediaObjectManager extends AbstractManager
{
}
