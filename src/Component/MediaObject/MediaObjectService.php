<?php

declare(strict_types=1);

namespace App\Component\MediaObject;

use App\Entity\MediaObject;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpKernel\KernelInterface;

readonly class MediaObjectService
{
    public function __construct(
        private Filesystem      $filesystem,
        private KernelInterface $kernel,
    ) {
    }

    public function mediaUploader(string $fileName, string $fileType, string $fileLocationPath): MediaObject
    {
        $tempDir = sys_get_temp_dir();
        $originalFilePath = $this->kernel->getProjectDir() . $fileLocationPath . $fileName;
        $tempFilePath = $tempDir . '/' . $fileName;
        $this->filesystem->copy($originalFilePath, $tempFilePath, true);

        $file = new UploadedFile(
            $tempFilePath,
            'file.' . explode('/' , $fileType)[1],   // 'file.jpg'
            $fileType,                              // 'image/png'
            null,
            true
        );

        $media = new MediaObject();
        $media->file = $file;

        return $media;
    }
}