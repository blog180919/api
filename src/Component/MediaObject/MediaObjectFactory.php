<?php

declare(strict_types=1);

namespace App\Component\MediaObject;

use App\Entity\MediaObject;
use Symfony\Component\HttpFoundation\File\File;

class MediaObjectFactory
{
    public function create(File $file): MediaObject
    {
        $mediaObject = new MediaObject();
        $mediaObject->file = $file;
        return $mediaObject;
    }
}
