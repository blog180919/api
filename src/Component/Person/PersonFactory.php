<?php

declare(strict_types=1);

namespace App\Component\Person;

use App\Entity\Person;
use App\Entity\User;
use DateTime;

class PersonFactory
{
    public function create(string $firstName, User $user, ?string $lastName = null, ?\DateTimeInterface $birthdate = null): Person
    {
        $person = new Person();
        $person->setFirstName($firstName);
        $person->setCreatedAt(new DateTime());
        $person->setCreatedBy($user);

        if ($lastName !== null) {
            $person->setLastName($lastName);
        }

        if ($birthdate !== null) {
            $person->setBirthdate($birthdate);
        }

        return $person;
    }
}
