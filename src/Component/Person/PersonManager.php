<?php

declare(strict_types=1);

namespace App\Component\Person;
use App\Component\Core\AbstractManager;
use App\Entity\Person;

/**
 * Class PersonManager
 *
 * @method save(Person $person, bool $needToFlush = false): void
 * @package App\Component\Person
 */
class PersonManager extends AbstractManager
{
}