<?php

declare(strict_types=1);

namespace App\Component\CommentLike;

use App\Entity\Comment;
use App\Entity\CommentLike;
use App\Entity\User;

class CommentLikeFactory
{
    public function create(Comment $comment, User $user): CommentLike
    {
        return (new CommentLike())
            ->setComment($comment)
            ->setCreatedAt(new \DateTime())
            ->setCreatedBy($user);
    }
}
