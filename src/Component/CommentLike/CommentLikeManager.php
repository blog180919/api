<?php

declare(strict_types=1);

namespace App\Component\CommentLike;

use App\Component\Core\AbstractManager;
use App\Entity\BlogLike;
use App\Entity\CommentLike;

/**
 * Class CommentLikeManager
 *
 * @method save(CommentLike $commentLike, bool $needToFlush = false): void
 * @package App\Component\CommentLike
 */
class CommentLikeManager extends AbstractManager
{
}