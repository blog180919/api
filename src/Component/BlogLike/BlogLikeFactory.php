<?php

declare(strict_types=1);

namespace App\Component\BlogLike;

use App\Entity\Blog;
use App\Entity\BlogLike;
use App\Entity\User;

class BlogLikeFactory
{
    public function create(Blog $blog, User $user): BlogLike
    {
        return (new BlogLike())
            ->setBlog($blog)
            ->setCreatedBy($user)
            ->setCreatedAt(new \DateTime());
    }
}