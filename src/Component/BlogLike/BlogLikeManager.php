<?php

declare(strict_types=1);

namespace App\Component\BlogLike;

use App\Component\Core\AbstractManager;
use App\Entity\BlogLike;

/**
 * Class BlogLikeManager
 *
 * @method save(BlogLike $blogLike, bool $needToFlush = false): void
 * @package App\Component\BlogLike
 */
class BlogLikeManager extends AbstractManager
{
}