<?php

declare(strict_types=1);

namespace App\Component\User\Dtos;

use Symfony\Component\Serializer\Annotation\Groups;

class UserDto
{
    public function __construct(
        #[Groups(['user:write'])]
        private readonly string $firstName,

        #[Groups(['user:write'])]
        private readonly ?string $lastName,

        #[Groups(['user:write'])]
        private readonly ?\DateTimeInterface $birthdate,

        #[Groups(['user:write'])]
        private readonly string $email,

        #[Groups(['user:write'])]
        private readonly string $password
    )
    {
    }

    public function getBirthdate(): ?\DateTimeInterface
    {
        return $this->birthdate;
    }

    public function getFirstName(): string
    {
        return $this->firstName;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getPassword(): string
    {
        return $this->password;
    }
}
