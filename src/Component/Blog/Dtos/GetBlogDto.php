<?php

declare(strict_types=1);

namespace App\Component\Blog\Dtos;

use App\Entity\Category;
use App\Entity\MediaObject;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Serializer\Annotation\Groups;

class GetBlogDto
{
    public function __construct(
        #[Groups(['blog:read', 'blogs:read'])]
        private readonly int $id,

        #[Groups(['blog:read', 'blogs:read'])]
        private readonly string $title,

        #[Groups(['blog:read', 'blogs:read'])]
        private readonly string     $description,

        #[Groups(['blog:read', 'blogs:read'])]
        private readonly string      $text,

        #[Groups(['blog:read', 'blogs:read'])]
        private readonly Category    $category,

        #[Groups(['blog:read'])]
        private readonly Collection  $blogLikes,

        #[Groups(['blog:read', 'blogs:read'])]
        private readonly int         $likesCount,

        #[Groups(['blog:read'])]
        private readonly Collection  $comments,

        #[Groups(['blog:read', 'blogs:read'])]
        private readonly int         $commentsCount,

        #[Groups(['blog:read', 'blogs:read'])]
        private readonly MediaObject $image,
    )
    {
    }

    public function getComments(): Collection
    {
        return $this->comments;
    }

    public function getBlogLikes(): Collection
    {
        return $this->blogLikes;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function getText(): string
    {
        return $this->text;
    }

    public function getCategory(): Category
    {
        return $this->category;
    }

    public function getCommentsCount(): int
    {
        return $this->commentsCount;
    }

    public function getLikesCount(): int
    {
        return $this->likesCount;
    }

    public function getImage(): MediaObject
    {
        return $this->image;
    }
}
