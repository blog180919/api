<?php

declare(strict_types=1);

namespace App\Component\Blog\Dtos;

use App\Entity\Category;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Serializer\Annotation\Groups;

readonly class CreateBlogDto
{
    public function __construct(
        #[Groups(['blog:write'])]
        private string $title,

        #[Groups(['blog:write'])]
        private string $description,

        #[Groups(['blog:write'])]
        private string $text,

        #[Groups(['blog:write'])]
        private Category $category,

        #[Groups(['blog:write'])]
        private File $image,
    ){
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function getText(): string
    {
        return $this->text;
    }

    public function getCategory(): Category
    {
        return $this->category;
    }

    public function getImage(): File
    {
        return $this->image;
    }
}