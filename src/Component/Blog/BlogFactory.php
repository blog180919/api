<?php

declare(strict_types=1);

namespace App\Component\Blog;

use App\Entity\Blog;
use App\Entity\Category;
use App\Entity\MediaObject;

class BlogFactory
{
    public function create(
        string $title,
        string $description,
        string $text,
        Category $category,
        MediaObject $image
    ): Blog
    {
        return (new Blog())
            ->setTitle($title)
            ->setDescription($description)
            ->setText($text)
            ->setCategory($category)
            ->setImage($image)
            ->setCreatedAt(new \DateTime());
    }
}