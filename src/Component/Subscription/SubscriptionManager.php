<?php

declare(strict_types=1);

namespace App\Component\Subscription;

use App\Component\Core\AbstractManager;
use App\Entity\BlogLike;
use App\Entity\Subscription;

/**
 * Class SubscriptionManager
 *
 * @method save(Subscription $subscription, bool $needToFlush = false): void
 * @package App\Component\Subscription
 */
class SubscriptionManager extends AbstractManager
{
}