<?php

declare(strict_types=1);

namespace App\Component\Subscription;

use App\Entity\Subscription;
use App\Entity\User;

class SubscriptionFactory
{
    public function create(User $toUser, User $user): Subscription
    {
        return (new Subscription())
            ->setToUser($toUser)
            ->setCreatedAt(new \DateTime())
            ->setCreatedBy($user);
    }
}
