<?php

namespace App\DataFixtures;

use App\Component\Person\PersonFactory;
use App\Component\User\UserFactory;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserFixtures extends Fixture
{
    public const ADMIN = 'admin';
    public const DEFAULT_USERS = [
        [
            'email' => 'user@example.com',
            'password' => 'string',
            'firstName' => 'Akmal',
            'lastName' => 'Kadirov'
        ],
        [
            'email' => 'user2@example.com',
            'password' => 'string',
            'firstName' => 'Rustam',
            'lastName' => 'Axmedov'
        ],
        [
            'email' => 'user3@example.com',
            'password' => 'string',
            'firstName' => 'Abdusattor',
            'lastName' => 'Abdusattorov'
        ],
        [
            'email' => 'user4@example.com',
            'password' => 'string',
            'firstName' => 'Sardor',
            'lastName' => 'Mallayev'
        ],
        [
            'email' => 'user5@example.com',
            'password' => 'string',
            'firstName' => 'Alisher',
            'lastName' => 'Sattorov'
        ],
        [
            'email' => 'user6@example.com',
            'password' => 'string',
            'firstName' => 'Abduqayum',
            'lastName' => 'Sotvoldiyev'
        ],
        [
            'email' => 'user7@example.com',
            'password' => 'string',
            'firstName' => 'Bilolxon',
            'lastName' => 'Nurmatov'
        ],
        [
            'email' => 'user8@example.com',
            'password' => 'string',
            'firstName' => 'Shuhratullo',
            'lastName' => 'Shukurov'
        ],
        [
            'email' => 'user9@example.com',
            'password' => 'string',
            'firstName' => 'To\'lqin',
            'lastName' => 'Muxtorov'
        ],
        [
            'email' => 'user10@example.com',
            'password' => 'string',
            'firstName' => 'Oybek',
            'lastName' => 'Salomov'
        ],
    ];

    public function __construct(
        private readonly UserFactory                 $userFactory,
        private readonly PersonFactory               $personFactory,
        private readonly UserPasswordHasherInterface $passwordHasher,
    )
    {
    }

    public function load(ObjectManager $manager): void
    {
        $admin = $this->createAdmin();
        $personForAdmin = $this->personFactory->create('Ma\'ruf', $admin, 'Sheraliyev', new \DateTime('27.11.1995'));
        $manager->persist($personForAdmin);

        foreach (self::DEFAULT_USERS as $user) {
            $newUser = $this->userFactory->create($user['email'], $user['password']);
            $newUser->setRoles(['ROLE_USER']);
            $personForUser = $this->personFactory->create($user['firstName'], $newUser, $user['lastName'], new \DateTime('01.01.2000'));
            $manager->persist($personForUser);
        }

        $manager->flush();

        $this->addReference(self::ADMIN, $admin);
    }

    private function createAdmin(): User
    {
        $admin = new User();
        $admin->setEmail('admin@example.com');
        $admin->setPassword($this->passwordHasher->hashPassword($admin, 'passwd'));
        $admin->setRoles(['ROLE_ADMIN']);
        $admin->setCreatedAt(new \DateTime());

        return $admin;
    }
}
