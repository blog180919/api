<?php

namespace App\DataFixtures;

use App\Component\Blog\BlogFactory;
use App\Component\MediaObject\MediaObjectService;
use App\Repository\CategoryRepository;
use App\Repository\UserRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class BlogFixtures extends Fixture implements DependentFixtureInterface
{
    public const DEFAULT_BLOGS = [
        [
            'title' => 'PHP',
            'description' => 'Personal home page',
            'category' => 8,
            'createdBy' => 2,
            'img' => 'php.png',
            'text' => 'Hozirgi internetlashgan zamonda bu til eng muhim daturlash tillaridan biri hisoblanadi. Ma’lumot uchun: internetdagi web dasturlar va saytlarning 80% shu tilda yozilgan. Shu jumladan Facebook ham aynan shu tilda yozilgan.'
        ],
        [
            'title' => 'HTML',
            'description' => 'Giper matnli belgilash tili',
            'category' => 8,
            'createdBy' => 2,
            'img' => 'html.png',
            'text' => 'HyperText Markup Language yoki oddiyroq qilib aytganda HTML bu veb-brauzerda ko\'rsatish uchun mo\'ljallangan hujjatlar uchun standart belgilash tilidir. HTML tili taxminan 1991—1992-yillarda Yevropa Yadroviy Tadqiqotlar Markazida ishlovchi britaniyalik mutaxassis Tim Berners-Lee tomonidan ishlab chiqilgan. Dastlab bu til mutaxassislar uchun hujjat tayyorlash vositasi sifatida yaratilgan.'
        ],
        [
            'title' => 'JavaScript',
            'description' => 'Brauzerlar uchun yagona til',
            'category' => 8,
            'createdBy' => 2,
            'img' => 'js.png',
            'text' => 'Ushbu til brauzerlar uchun ishlab chiqilgan bo’lib, hozirgi kunda deyarli barcha dasturlash yo’nalishlarida keng qo’llaniladi. Brauzerlarda yagona dasturlash tili hisoblanadi. Hozir mobil dasturlarni ham ushbu tilda yozish urf bo’lmoqda.'
        ],
        [
            'title' => 'MySQL',
            'description' => 'Ma\'lumotlar omborini boshqarish tizimlaridan biri',
            'category' => 8,
            'createdBy' => 3,
            'img' => 'mySQL.png',
            'text' => 'Har bir dastur ma’lumotlarni qayerdadir saqlashi kerak. Misol uchun foydalanuvchining elektron manzili, ism-sharfi va hokazo. Buning uchun MySQL ma’lumotlar ombori tizimi bor. Ma’lumot uchun: Facebook barcha ma’lumotlarni aynan MySQL tizimida saqlaydi.'
        ],
        [
            'title' => 'CSS',
            'description' => 'Kaskadlangan stillar jadvali',
            'category' => 8,
            'createdBy' => 4,
            'img' => 'css.png',
            'text' => 'CSS elementlarni ekranda, qogʻozda va yoki boshqa medialarda koʻrinishini tasvirlaydi. CSS veb sahifalarga har xil stillar berish uchun ishlatiladi. Hamda bir vaqtning oʻzida bir nechta veb sahifalarni dizaynini oʻzgartirish mumkin. Biror bir veb sahifani turli xil qurilmalarda turli xil koʻrinishini ham taʼminlaydi. CSS file .css formati orqali tashqi xotira saqlab qoʻyishingiz va kezi kelganda veb sahifaning HTML kodini oʻzgartirmasdan, faqat CSS faylni oʻzgartirish orqali veb sahifaga yangidan koʻrinish berishimiz mumkin. Qisqacha qilib aytadigan boʻlsak, HTML faqat veb sahifani kodlarini yozish uchun ishlatiladi, HTML da ham veb sahifaga dizayn bersa boʻladi, lekin bu juda uzoq jarayon talab qilgani bois, CSS bu vazifani oʻz boʻyniga olgan.'
        ],
        [
            'title' => 'Symfony',
            'description' => 'Web dasturlarning backend qismini yozish uchun kuchli frameworklardan biri',
            'category' => 8,
            'createdBy' => 5,
            'img' => 'Symfony.png',
            'text' => 'Backend uchun eng kuchli freymvork. Dunyoda 3 ta mashhur freymvorklar bor. Symfony, Yii va Laravel. Laravel ko’proq Kanada davlatida ishlatiladi. Symfony esa Yevropa davlatlarida. Yii freymvorki Rossiya va O’zbekistonda. Lekin Yii shunchalik eskirdiki ushbu davlatlar ham aynan Symfony freymvorkiga o’tishni boshlashdi. Backend dastur yaratish uchun ushbu freymvorkka teng keladigani yo’q.'
        ],
    ];

    public function __construct(
        private readonly BlogFactory $blogFactory,
        private readonly MediaObjectService $mediaObjectService,
        private readonly CategoryRepository $categoryRepository,
        private readonly UserRepository $userRepository
    )
    {
    }

    public function load(ObjectManager $manager): void
    {
        $admin = $this->getReference(UserFixtures::ADMIN);

        foreach (self::DEFAULT_BLOGS as $blog) {
            $picture = $this->mediaObjectService->mediaUploader(
                $blog['img'],
                'image/png',
                '/public/defaultMedia/images/'
            );

            $manager->persist($picture);

            $newBlog = $this->blogFactory->create(
                $blog['title'],
                $blog['description'],
                $blog['text'],
                $this->categoryRepository->find($blog['category']),
                $picture
            );

            $newBlog->setCreatedBy($this->userRepository->find($blog['createdBy']));
            $manager->persist($newBlog);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [UserFixtures::class, CategoryFixtures::class];
    }
}
