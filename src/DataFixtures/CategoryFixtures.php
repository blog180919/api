<?php

namespace App\DataFixtures;

use App\Component\Person\PersonFactory;
use App\Component\User\UserFactory;
use App\Component\User\UserWithPersonServes;
use App\Entity\Category;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class CategoryFixtures extends Fixture implements DependentFixtureInterface
{
    public const DEFAULT_CATEGORIES = [
        'Technology',
        'Politics',
        'Sports',
        'Finance and Business',
        'Education',
        'Articles on Various Fields of Expertise',
        'Design',
        'Programming',
        'Tourism',
        'Photography and Art',
    ];

    public function load(ObjectManager $manager): void
    {
        $admin = $this->getReference(UserFixtures::ADMIN);

        foreach (self::DEFAULT_CATEGORIES as $category) {
            $newCategory = new Category();
            $newCategory->setName($category);
            $newCategory->setCreatedBy($admin);
            $newCategory->setCreatedAt(new \DateTime());

            $manager->persist($newCategory);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [UserFixtures::class];
    }
}
