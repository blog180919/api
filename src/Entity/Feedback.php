<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Post;
use ApiPlatform\OpenApi\Model\Operation;
use ApiPlatform\OpenApi\Model\RequestBody;
use App\Entity\Interfaces\CreatedAtSettableInterface;
use App\Entity\Interfaces\CreatedBySettableInterface;
use App\Entity\Interfaces\DeletedAtSettableInterface;
use App\Entity\Interfaces\DeletedBySettableInterface;
use App\Entity\Interfaces\UpdatedAtSettableInterface;
use App\Entity\Interfaces\UpdatedBySettableInterface;
use App\Repository\FeedbackRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: FeedbackRepository::class)]
#[ApiResource(
    operations: [
        new GetCollection(),
        new Post(
            openapi: new Operation(
                summary: 'Write us uchun ya\'ni bizga yozing.',
                requestBody: new RequestBody(
                    content: new \ArrayObject([
                        'application/json' => [
                            'schema' => [
                                'type' => 'object',
                                'properties' => [
                                    'fullName' => ['type' => 'string'],
                                    'email' => ['type' => 'string'],
                                    'text' => ['type' => 'string']
                                ]
                            ],
                            'example' => [
                                'fullName' => 'Abdusattor Abdusattorov',
                                'email' => 'user@example.com',
                                'text' => 'Nimadir va qanqadir text.'
                            ]
                        ]
                    ])
                )
            )
        ),
        new Get()
    ],
    normalizationContext: ['groups' => ['feedback:read', 'feedbacks:read']],
    denormalizationContext: ['groups' => ['feedback:write']],
)]
class Feedback implements CreatedAtSettableInterface
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(['feedback:read', 'feedback:write', 'feedbacks:read'])]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Groups(['feedback:read', 'feedback:write', 'feedbacks:read'])]
    private ?string $fullName = null;

    #[ORM\Column(length: 255)]
    #[Groups(['feedback:read', 'feedback:write', 'feedbacks:read'])]
    private ?string $email = null;

    #[ORM\Column(type: Types::TEXT)]
    #[Groups(['feedback:read', 'feedback:write', 'feedbacks:read'])]
    private ?string $text = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    #[Groups(['feedbacks:read'])]
    private ?\DateTimeInterface $createdAt = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFullName(): ?string
    {
        return $this->fullName;
    }

    public function setFullName(string $fullName): self
    {
        $this->fullName = $fullName;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText(string $text): self
    {
        $this->text = $text;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }
}
