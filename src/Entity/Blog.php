<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\Put;
use ApiPlatform\OpenApi\Model\Operation;
use ApiPlatform\OpenApi\Model\RequestBody;
use App\Component\Blog\Dtos\CreateBlogDto;
use App\Component\Blog\Dtos\GetBlogsDto;
use App\Controller\Blog\CreateBlogAction;
use App\Controller\Blog\GetBlogAction;
use App\Controller\Blog\GetBlogsAction;
use App\Controller\Blog\GetBlogsByCommentsCount;
use App\Controller\Blog\GetBlogsByFollowingsAction;
use App\Controller\Blog\GetBlogsByNewestAction;
use App\Controller\Blog\GetBlogsByTrendings;
use App\Controller\Blog\GetBlogsByFollowingUsersAction;
use App\Entity\Interfaces\CreatedAtSettableInterface;
use App\Entity\Interfaces\CreatedBySettableInterface;
use App\Entity\Interfaces\DeletedAtSettableInterface;
use App\Entity\Interfaces\DeletedBySettableInterface;
use App\Entity\Interfaces\UpdatedAtSettableInterface;
use App\Entity\Interfaces\UpdatedBySettableInterface;
use App\Repository\BlogRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: BlogRepository::class)]
#[ApiResource(
    operations: [
        new GetCollection(
            controller: GetBlogsAction::class,
            openapi: new Operation(
                summary: 'Barcha bloglarni olish. Filterlash ham mumkin authorName va categoryId\'lari bo\'yicha'
            ),
            normalizationContext: ['groups' => ['blogs:read']],
            output: GetBlogsDto::class,
        ),
        new GetCollection(
            uriTemplate: '/blogs/by_followings',
            controller: GetBlogsByFollowingUsersAction::class,
            openapi: new Operation(
                summary: 'Obuna bo\'lgan foydalanuvchilarining bloglarini olish'
            ),
            normalizationContext: ['groups' => ['blogs:read']],
        ),
        new GetCollection(
            uriTemplate: '/blogs/by_trendings',
            controller: GetBlogsByTrendings::class,
            openapi: new Operation(
                summary: 'Layklar soni bo\'yicha bloglarini olish'
            ),
            normalizationContext: ['groups' => ['blogs:read']],
        ),
        new GetCollection(
            uriTemplate: '/blogs/by_comments_count',
            controller: GetBlogsByCommentsCount::class,
            openapi: new Operation(
                summary: 'Izohlari miqdori bo\'yicha bloglarini olish'
            ),
            normalizationContext: ['groups' => ['blogs:read']],
        ),
        new Post(
            inputFormats: ['multipart' => ['multipart/form-data']],
            controller: CreateBlogAction::class,
            openapi: new Operation(
                summary: 'Yangi blog yaratish.'
            ),
            input: CreateBlogDto::class,
        ),
        new Get(
            controller: GetBlogAction::class
        ),
        new Put(),
        new Delete(),
        new Patch()
    ],
    normalizationContext: ['groups' => ['blog:read', 'blogs:read']],
    denormalizationContext: ['groups' => ['blog:write']],
)]
class Blog implements
    CreatedAtSettableInterface,
    CreatedBySettableInterface,
    UpdatedAtSettableInterface,
    UpdatedBySettableInterface,
    DeletedAtSettableInterface,
    DeletedBySettableInterface
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(['blogs:read'])]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Groups(['blog:read', 'blogs:read', 'blog:write'])]
    private ?string $title = null;

    #[ORM\Column(length: 255)]
    #[Groups(['blog:read', 'blogs:read', 'blog:write'])]
    private ?string $description = null;

    #[ORM\Column(type: Types::TEXT)]
    #[Groups(['blog:read', 'blogs:read', 'blog:write'])]
    private ?string $text = null;

    #[ORM\ManyToOne(inversedBy: 'blogs')]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(['blog:read', 'blogs:read', 'blog:write'])]
    private ?Category $category = null;

    #[ORM\OneToMany(mappedBy: 'blog', targetEntity: BlogLike::class)]
    #[Groups(['blog:read'])]
    private Collection $blogLikes;

    #[ORM\OneToMany(mappedBy: 'blog', targetEntity: Comment::class)]
    #[Groups(['blog:read'])]
    private Collection $comments;

    #[ORM\ManyToOne]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(['blog:read', 'blogs:read', 'blog:write'])]
    private ?MediaObject $image = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    #[Groups(['blogs:read'])]
    private ?\DateTimeInterface $createdAt = null;

    #[ORM\ManyToOne(inversedBy: 'blogs')]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(['blogs:read'])]
    private ?User $createdBy = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    #[Groups(['blogs:read'])]
    private ?\DateTimeInterface $updatedAt = null;

    #[ORM\OneToOne(cascade: ['persist', 'remove'])]
    #[Groups(['blogs:read'])]
    private ?User $updatedBy = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $deletedAt = null;

    #[ORM\OneToOne(cascade: ['persist', 'remove'])]
    private ?User $deletedBy = null;

    public function __construct()
    {
        $this->blogLikes = new ArrayCollection();
        $this->comments = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText(string $text): self
    {
        $this->text = $text;

        return $this;
    }

    public function getCategory(): ?Category
    {
        return $this->category;
    }

    public function setCategory(?Category $category): self
    {
        $this->category = $category;

        return $this;
    }

    /**
     * @return Collection<int, BlogLike>
     */
    public function getBlogLikes(): Collection
    {
        return $this->blogLikes;
    }

    public function addBlogLike(BlogLike $blogLike): self
    {
        if (!$this->blogLikes->contains($blogLike)) {
            $this->blogLikes->add($blogLike);
            $blogLike->setBlog($this);
        }

        return $this;
    }

    public function removeBlogLike(BlogLike $blogLike): self
    {
        if ($this->blogLikes->removeElement($blogLike)) {
            // set the owning side to null (unless already changed)
            if ($blogLike->getBlog() === $this) {
                $blogLike->setBlog(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Comment>
     */
    public function getComments(): Collection
    {
        return $this->comments;
    }

    public function addComment(Comment $comment): self
    {
        if (!$this->comments->contains($comment)) {
            $this->comments->add($comment);
            $comment->setBlog($this);
        }

        return $this;
    }

    public function removeComment(Comment $comment): self
    {
        if ($this->comments->removeElement($comment)) {
            // set the owning side to null (unless already changed)
            if ($comment->getBlog() === $this) {
                $comment->setBlog(null);
            }
        }

        return $this;
    }

    public function getImage(): ?MediaObject
    {
        return $this->image;
    }

    public function setImage(?MediaObject $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getCreatedBy(): ?User
    {
        return $this->createdBy;
    }

    public function setCreatedBy(UserInterface $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getUpdatedBy(): ?User
    {
        return $this->updatedBy;
    }

    public function setUpdatedBy(?UserInterface $updatedBy): self
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTimeInterface $deletedAt): self
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    public function getDeletedBy(): ?User
    {
        return $this->deletedBy;
    }

    public function setDeletedBy(?UserInterface $deletedBy): self
    {
        $this->deletedBy = $deletedBy;

        return $this;
    }
}
