<?php

declare(strict_types=1);

namespace App\Controller\Subscription;

use App\Component\Subscription\SubscriptionFactory;
use App\Component\Subscription\SubscriptionManager;
use App\Controller\Base\AbstractController;
use App\Entity\Subscription;
use App\Repository\SubscriptionRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class CreateOrDeleteFollower extends AbstractController
{
    public function __invoke(
        Subscription           $data,
        SubscriptionRepository $subscriptionRepository,
        SubscriptionFactory    $subscriptionFactory,
        SubscriptionManager    $subscriptionManager
    ): Response
    {
        $follower = $subscriptionRepository->findFollowersByUser($data->getToUser(), $this->getUser());

        if ($data->getToUser()->getId() === $this->getUser()->getId()) {
            throw new BadRequestHttpException('You cannot follow for yourself');
        }

        if ($follower === null) {
            $newFollower = $subscriptionFactory->create($data->getToUser(), $this->getUser());
            $subscriptionManager->save($newFollower, true);
            return $this->responseNormalized($newFollower);
        } else {
            $subscriptionRepository->remove($follower, true);
            return $this->responseEmpty();
        }
    }
}
