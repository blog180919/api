<?php

declare(strict_types=1);

namespace App\Controller\Blog;

use App\Controller\Base\AbstractController;
use App\Repository\BlogRepository;
use App\Repository\SubscriptionRepository;

class GetBlogsByFollowingUsersAction extends AbstractController
{
    public function __invoke(BlogRepository $blogRepository, SubscriptionRepository $subscriptionRepository): array
    {
        $subscriptions = $subscriptionRepository->findBy(['createdBy' => $this->getUser()->getId()]);
        $ids = [];

        foreach ($subscriptions as $subscription) {
            $ids[] = $subscription->getToUser()->getId();
        }

        return $blogRepository->findByUsers($ids);
    }
}
