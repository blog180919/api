<?php

declare(strict_types=1);

namespace App\Controller\Blog;

use ApiPlatform\Validator\ValidatorInterface;
use App\Component\Blog\BlogFactory;
use App\Component\MediaObject\MediaObjectFactory;
use App\Component\MediaObject\MediaObjectManager;
use App\Component\User\CurrentUser;
use App\Controller\Base\AbstractController;
use App\Entity\Blog;
use App\Repository\CategoryRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\SerializerInterface;

class CreateBlogAction extends AbstractController
{
    public function __construct(
        SerializerInterface                 $serializer,
        ValidatorInterface                  $validator,
        CurrentUser                         $currentUser,
        private readonly CategoryRepository $categoryRepository,
        private readonly MediaObjectFactory $mediaObjectFactory,
        private readonly MediaObjectManager $mediaObjectManager,
        private readonly BlogFactory        $blogFactory
    )
    {
        parent::__construct($serializer, $validator, $currentUser);
    }

    public function __invoke(Request $request): Blog
    {
        $image = $request->files->get('image');
        $title = $request->request->get('title');
        $description = $request->request->get('description');
        $text = $request->request->get('text');
        $category = $request->request->get('category');
        $findCategory = $this->categoryRepository->find(intval(substr($category, strrpos($category, '/') + 1)));

        $mediaObject = $this->mediaObjectFactory->create($image);
        $this->mediaObjectManager->save($mediaObject, true);

        return $this->blogFactory->create($title, $description, $text, $findCategory, $mediaObject);
    }
}
