<?php

declare(strict_types=1);

namespace App\Controller\Blog;

use App\Component\Blog\Dtos\GetBlogsDto;
use App\Controller\Base\AbstractController;
use App\Repository\BlogRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class GetBlogsAction extends AbstractController
{
    public function __invoke(BlogRepository $blogRepository, Request $request): Response
    {
        $page = $request->query->getInt('page', 1);
        $itemsPerPage = $request->query->getInt('items-per-page', 20);
        $categoryId = $request->query->getInt('category');
        $authorName = $request->query->get('authorName', '');

        $blogs = $blogRepository->blogsPagination($authorName, $page, $itemsPerPage, $categoryId);
        $blogsTotalItems = $blogRepository->getBlogsCount();
        $result = [];

        foreach ($blogs as $blog) {
            $likesCount = $blogRepository->getLikesCount($blog->getId());
            $commentsCount = $blogRepository->getCommentsCount($blog->getId());
            $result[] = new GetBlogsDto(
                $blog->getId(),
                $blog->getTitle(),
                $blog->getDescription(),
                $blog->getText(),
                $blog->getCategory(),
                (int)$commentsCount,
                (int)$likesCount,
                $blog->getImage(),
                $blog->getCreatedAt(),
                $blog->getCreatedBy()
            );
        }

        return $this->responseNormalized([
            'hydra:members' => $result,
            'hydra:totalItems' => $blogsTotalItems
        ]);
    }
}
