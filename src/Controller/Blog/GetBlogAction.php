<?php

declare(strict_types=1);

namespace App\Controller\Blog;

use App\Component\Blog\Dtos\GetBlogDto;
use App\Controller\Base\AbstractController;
use App\Repository\BlogRepository;

class GetBlogAction extends AbstractController
{
    public function __invoke(BlogRepository $blogRepository, int $id)
    {
        $blog = $blogRepository->find($id);
        $commentsCount = $blogRepository->getCommentsCount($id);
        $likesCount = $blogRepository->getLikesCount($id);

        $result[] = new GetBlogDto(
            $blog->getId(),
            $blog->getTitle(),
            $blog->getDescription(),
            $blog->getText(),
            $blog->getCategory(),
            $blog->getBlogLikes(),
            (int)$likesCount,
            $blog->getComments(),
            (int)$commentsCount,
            $blog->getImage()
        );

        return $result;
    }
}
