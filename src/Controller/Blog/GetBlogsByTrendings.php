<?php

declare(strict_types=1);

namespace App\Controller\Blog;

use App\Controller\Base\AbstractController;
use App\Repository\BlogRepository;

class GetBlogsByTrendings extends AbstractController
{
    public function __invoke(BlogRepository $blogRepository): array
    {
        return $blogRepository->findBlogsByTrending();
    }
}