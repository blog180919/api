<?php

declare(strict_types=1);

namespace App\Controller\BlogLike;

use App\Component\BlogLike\BlogLikeFactory;
use App\Component\BlogLike\BlogLikeManager;
use App\Controller\Base\AbstractController;
use App\Entity\BlogLike;
use App\Repository\BlogLikeRepository;
use Symfony\Component\HttpFoundation\Response;

class CreateBlogLikeAction extends AbstractController
{
    public function __invoke(
        BlogLike $data,
        BlogLikeFactory $blogLikeFactory,
        BlogLikeRepository $blogLikeRepository,
        BlogLikeManager $blogLikeManager
    ): Response
    {
        $blogLike = $blogLikeRepository->findBlogLikesByBlog($data->getBlog(), $this->getUser());

        if ($blogLike === null) {
            $newBlogLike = $blogLikeFactory->create($data->getBlog(), $this->getUser());
            $blogLikeManager->save($newBlogLike, true);
            return $this->responseNormalized($newBlogLike);
        } else {
            $blogLikeRepository->remove($blogLike, true);
            return $this->responseEmpty();
        }
    }
}
