<?php

declare(strict_types=1);

namespace App\Controller\CommentLike;

use App\Component\BlogLike\BlogLikeManager;
use App\Component\CommentLike\CommentLikeFactory;
use App\Component\CommentLike\CommentLikeManager;
use App\Controller\Base\AbstractController;
use App\Entity\BlogLike;
use App\Entity\CommentLike;
use App\Repository\BlogLikeRepository;
use App\Repository\CommentLikeRepository;
use Symfony\Component\HttpFoundation\Response;

class CreateCommentLikeAction extends AbstractController
{
    public function __invoke(
        CommentLike $data,
        CommentLikeRepository $commentLikeRepository,
        CommentLikeFactory $commentLikeFactory,
        CommentLikeManager $commentLikeManager
    ): Response
    {
        $commentLike = $commentLikeRepository->findCommentLikesByComment($data->getComment(), $this->getUser());

        if ($commentLike === null) {
            $newCommentLike = $commentLikeFactory->create($data->getComment(), $this->getUser());
            $commentLikeManager->save($newCommentLike, true);
            return $this->responseNormalized($newCommentLike);
        } else {
            $commentLikeRepository->remove($commentLike, true);
            return $this->responseEmpty();
        }
    }
}
