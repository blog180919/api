<?php

declare(strict_types=1);

namespace App\Controller;

use App\Component\Person\PersonFactory;
use App\Component\Person\PersonManager;
use App\Component\User\Dtos\UserDto;
use App\Component\User\UserFactory;
use App\Component\User\UserManager;
use App\Controller\Base\AbstractController;
use App\Entity\User;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class CreateUserController
 *
 * @package App\Controller
 */
class UserCreateAction extends AbstractController
{
    public function __invoke(
        Request $request,
        UserFactory $userFactory,
        UserManager $userManager,
        PersonFactory $personFactory,
        PersonManager $personManager
    ): User
    {
        /** @var UserDto $userDto */
        $userDto = $this->getDtoFromRequest($request,UserDto::class);

        $this->validate($userDto);

        $user = $userFactory->create($userDto->getEmail(), $userDto->getPassword());
        $userManager->save($user);

        $person = $personFactory->create($userDto->getFirstName(), $user, $userDto->getLastName(), $userDto->getBirthdate());
        $personManager->save($person, true);

        return $user;
    }
}
